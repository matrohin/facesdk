#define _CRT_SECURE_NO_WARNINGS

#include <algorithm>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>


#include "LFW_Dataset.h"


// static
std::string LFW_Dataset::getPath(
	const std::string &lfw_dir,
	const std::string &name,
	const int n)
{
	char tmp[10000];
	std::sprintf(
		tmp,
		"%s/%s/%s_%04d.jpg",
		lfw_dir.c_str(),
		name.c_str(),
		name.c_str(),
		n);

	return std::string(tmp);
}


LFW_Dataset::LFW_Dataset(
	const std::string pairs_filepath,
	const std::string lfw_dir)
{
	std::ifstream pairs_file(pairs_filepath.c_str());

	int folds_count = 0, fold_size = 0;
	pairs_file >> folds_count >> fold_size;

	typedef std::pair<std::string, int> name_id;



	std::vector<std::string> all_names;
	all_names.reserve(folds_count * fold_size * 3);


	std::vector<name_id> restricted_pairs;
	restricted_pairs.reserve(folds_count * fold_size * 4);

	for(int i = 0; i < folds_count; ++i)
	{
		// matches
		for(int j = 0; j < fold_size; ++j)
		{
			std::string name;
			int n1, n2;
			pairs_file >> name >> n1 >> n2;

			all_names.push_back(name);

			restricted_pairs.push_back(name_id(name, n1));
			restricted_pairs.push_back(name_id(name, n2));
		}

		// mismatches
		for(int j = 0; j < fold_size; ++j)
		{
			std::string name1, name2;
			int n1, n2;
			pairs_file >> name1 >> n1 >> name2 >> n2;

			all_names.push_back(name1);
			all_names.push_back(name2);

			restricted_pairs.push_back(name_id(name1, n1));
			restricted_pairs.push_back(name_id(name2, n2));
		}
	}
	pairs_file.close();


	std::sort(all_names.begin(), all_names.end());
	all_names.resize(std::unique(all_names.begin(), all_names.end()) - all_names.begin());


	std::vector<name_id> all;
	for(size_t i = 0; i < all_names.size(); ++i)
	{
		const int count = countPersonImages(all_names[i], lfw_dir);
		for(int j = 1; j <= count; ++j)
			all.push_back(name_id(all_names[i], j));
	}


	std::vector<int> restricted_pairs_idx(restricted_pairs.size());
	for(size_t i = 0; i < restricted_pairs.size(); ++i)
	{
		const int idx = std::lower_bound(
			all.begin(),
			all.end(),
			restricted_pairs[i]) - all.begin();

		restricted_pairs_idx[i] = idx;
	}


	// finally

	_all_paths.resize(all.size());
	for(size_t i = 0; i < all.size(); ++i)
		_all_paths[i] = getPath(lfw_dir, all[i].first, all[i].second);


	// making restricted folds
	int the_idx = 0; // index in restricted_pairs_idx vector
	_restricted_folds.resize(folds_count);
	for(int i = 0; i < folds_count; ++i)
	{
		for(int k = 0; k < 2; ++k)
		{
			_restricted_folds[i].pairs[k].resize(fold_size);
			for(int j = 0; j < fold_size; ++j)
			{
				_restricted_folds[i].pairs[k][j].first = restricted_pairs_idx[the_idx++];
				_restricted_folds[i].pairs[k][j].second = restricted_pairs_idx[the_idx++];
			}
		}
	}

}


// static
int LFW_Dataset::countPersonImages(
	const std::string &name,
	const std::string &lfw_dir)
{
	int count = 0;
	std::string path;
	for(;;)
	{
		path = getPath(lfw_dir, name, count + 1);
		std::ifstream file(path.c_str());

		if(!file.is_open())
			break;

		file.close();
		++count;
	}

	return count;
}




// virtual
std::string LFW_Dataset::getPath(const int i) const
{
	return _all_paths[i];
}



// virtual
int LFW_Dataset::getFoldsCount() const
{
	return _restricted_folds.size();
}



// virtual
IndexSource* LFW_Dataset::createIndexSource(
	std::vector<int> folds) const
{
	std::sort(folds.begin(), folds.end());
	folds.resize(std::unique(folds.begin(), folds.end()) - folds.begin());


	RestrictedIndexSource* result = new RestrictedIndexSource();

	for(size_t i = 0; i < folds.size(); ++i)
	{
		const lfw_restricted_fold &fold = _restricted_folds[folds[i]];

		for(int k = 0; k < 2; ++k)
			for(size_t j = 0; j < fold.pairs[k].size(); ++j)
				result->_data.pairs[k].push_back(fold.pairs[k][j]);
	}

	return result;

}


