#include <algorithm>

#include "IndexSource.h"

using std::size_t;

const std::vector<IndexSource::index_pair>& RestrictedIndexSource::all_matches() const
{
	return _data.matches();
}

const std::vector<IndexSource::index_pair>& RestrictedIndexSource::all_mismatches() const
{
	return _data.mismatches();
}
