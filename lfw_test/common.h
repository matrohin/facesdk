#ifndef __LFWTEST_SAMPLE_COMMON_H__
#define __LFWTEST_SAMPLE_COMMON_H__

#include <vector>
#include <utility>


// returns equal error rate
float extractInfo(
	const std::vector<std::pair<float, float> > &roc,  // vector<pair<far, tar> >
	const std::vector<float> target_fars,
	std::vector<float> &closest_fars,
	std::vector<float> &closest_tars);

std::vector<std::pair<float, float> > make_roc(
	std::vector<float> match_vector,
	std::vector<float> mismatch_vector);

#endif  // __LFWTEST_SAMPLE_COMMON_H__
