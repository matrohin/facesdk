
#include <algorithm>
#include <cmath>
#include <utility>
#include <vector>

#include "common.h"

using std::size_t;

// returns equal error rate
float extractInfo(
	const std::vector<std::pair<float, float> > &roc,  // vector<pair<far, tar> >
	const std::vector<float> target_fars,
	std::vector<float> &closest_fars,
	std::vector<float> &closest_tars)
{
	closest_fars.assign(target_fars.size(), -1);
	closest_tars.assign(target_fars.size(), -1);

	float result_eer = 0;
	float result_eer_diff = 1;

	for(size_t i = 0; i < roc.size(); ++i)
	{
		const float far = roc[i].first;
		const float tar = roc[i].second;

		for(size_t j = 0; j < target_fars.size(); ++j)
		{
			if(fabs(far - target_fars[j]) < fabs(closest_fars[j] - target_fars[j]))
			{
				closest_fars[j] = far;
				closest_tars[j] = tar;
			}
		}

		// check eer
		const float trr = 1 - far;

		const float diff = fabs(trr - tar);
		if(diff < result_eer_diff)
		{
			result_eer_diff = diff;
			result_eer = (trr + tar) * 0.5f;
		}
	}

	return result_eer;
}




// vector<pair<FAR, TAR> >
std::vector<std::pair<float, float> > make_roc(
	std::vector<float> match_vector,
	std::vector<float> mismatch_vector)
{
	std::sort(match_vector.begin(), match_vector.end());
	std::sort(mismatch_vector.begin(), mismatch_vector.end());

	std::vector<std::pair<float, float> > roc;

	std::vector<float> thresholds(match_vector.size() + mismatch_vector.size());
	std::merge(
		match_vector.begin(), match_vector.end(),
		mismatch_vector.begin(), mismatch_vector.end(),
		thresholds.begin());

	for(size_t i = 0; i < thresholds.size(); ++i)
	{
		const float threshold = thresholds[i];

		const size_t true_positive_cnt =
			match_vector.end() -
			std::lower_bound(
				match_vector.begin(),
				match_vector.end(),
				threshold);

		const size_t false_positive_cnt =
			mismatch_vector.end() -
			std::lower_bound(
				mismatch_vector.begin(),
				mismatch_vector.end(),
				threshold);

		roc.push_back(std::make_pair(
			false_positive_cnt / (float) mismatch_vector.size(),
			true_positive_cnt / (float) match_vector.size() ));
	}


	return roc;
}
