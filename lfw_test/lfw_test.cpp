#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>


#include <pbio/FacerecService.h>

#include "common.h"

#include "LFW_Dataset.h"


pbio::Template::Ptr getTemplate(
	const std::string file,
	pbio::Capturer &capturer,
	const pbio::Recognizer &recognizer,
	std::map<std::string, pbio::Template::Ptr> &memory)
{
	// check if we already processed this file
	std::map<std::string, pbio::Template::Ptr>::iterator iterator = memory.find(file);
	if(iterator != memory.end())
	{
		return iterator->second;
	}

	// first time for this file

	// loading image file content
	std::vector<uint8_t> file_data;

	std::ifstream sfile(file.c_str(), std::ios_base::binary);

	if(!sfile.is_open())
	{
		throw std::runtime_error("can't open file '" + file + "'.");
	}

	sfile.seekg(0, sfile.end);
	const size_t len = (size_t) sfile.tellg();

	sfile.seekg(0, sfile.beg);

	file_data.resize(len);

	sfile.read((char*)&file_data[0], len);

	if(!sfile.good())
	{
		throw std::runtime_error("error reading file '" + file + "'.");
	}

	// capture samples from image
	const std::vector<pbio::RawSample::Ptr> samples = capturer.capture(file_data.data(), file_data.size());

	pbio::Template::Ptr result;

	if(samples.size() == 1)
	{
		result = recognizer.processing(*samples[0]);
	}
	else
	{
		// on LFW images there must be exactly one face
		// so this is detection error
		// and here we do nothing
		// result well be NULL
	}

	// save result for this file
	memory[file] = result;

	return result;
}

int main (int argc, char const *argv[])
{
	try
	{
		std::cout << "\nLFW test\n\n";

		if (argc != 6)
		{
			std::cout << "\nUsage: ./lfw_test_sample <lfw_pairs_file> <lfw_dir> "
				"<path to facerec.dll> <path to conf directory> <recognizer config>\n"
				"  <lfw_pairs_file> - path to file from http://vis-www.cs.umass.edu/lfw/pairs.txt\n"
				"  <lfw_dir> - directory where you unpacked archive from http://vis-www.cs.umass.edu/lfw/lfw.tgz\n"
				"\n\n";
			return -1;
		}

		const std::string pairs_filepath = argv[1];
		const std::string lfw_dir = argv[2];
		const std::string facerec_lib_path = argv[3];
		const std::string facerec_conf_path = argv[4];
		const std::string recognizer_conf = argv[5];

		// open lfw dataset
		LFW_Dataset dataset(pairs_filepath, lfw_dir);

		// initialize work with facerec
		const pbio::FacerecService::Ptr service =
			pbio::FacerecService::createService(
				facerec_lib_path,
				facerec_conf_path);

		// create capturer
		// (lfw_static_face_capturer - it's a special config for LFW test, since in lfw faces have fixed positions on images)
		// (actually you can use any other capturers but test result can be worse since face detection errors)
		pbio::Capturer::Ptr capturer = service->createCapturer("lfw_static_face_capturer.xml");

		// create recognizer
		const pbio::Recognizer::Ptr recognizer = service->createRecognizer(recognizer_conf);


		// process images
		std::vector<float> match_scores;
		std::vector<float> mismatch_scores;

		for(int i = 0; i < dataset.getFoldsCount(); ++i)
		{
			std::vector<int> i_fold;
			i_fold.push_back(i);

			std::cout << "fold: " << i << std::endl;

			IndexSource* const idx_source = dataset.createIndexSource(i_fold);

			std::map<std::string, pbio::Template::Ptr> memory;

			const std::vector<std::pair<int, int> > matches = idx_source->all_matches();
			const std::vector<std::pair<int, int> > mismatches = idx_source->all_mismatches();

			for(int k = 0; k < 2; ++k)
			{
				const std::vector<std::pair<int, int> > &indexes = k ? matches : mismatches;
				std::vector<float> &scores = k ? match_scores : mismatch_scores;

				for(size_t j = 0; j < indexes.size(); ++j)
				{
					const pbio::Template::Ptr first = getTemplate(dataset.getPath(indexes[j].first), *capturer, *recognizer, memory);
					const pbio::Template::Ptr second = getTemplate(dataset.getPath(indexes[j].second), *capturer, *recognizer, memory);

					const float score = (first && second) ?
						( -recognizer->verifyMatch(*first, *second).distance ) :
						(-1e9);

					scores.push_back(score);

					std::cout << "\r: "  << (j + 1) << " / " << indexes.size() << std::flush;
				}
				std::cout << std::endl;
			}

			delete idx_source;
		}

		// compute ROC-curve
		const std::vector<std::pair<float, float> > roc = make_roc(match_scores, mismatch_scores);


		// extract TAR corresponding to some FAR values

		static const int target_far_cnt = 19;
		static const float target_fars[target_far_cnt] = {
			0.1,
			0.09,
			0.08,
			0.07,
			0.06,
			0.05,
			0.04,
			0.03,
			0.02,
			0.01,
			0.009,
			0.008,
			0.007,
			0.006,
			0.005,
			0.004,
			0.003,
			0.002,
			0.001
			};

		std::vector<float> closest_far, closest_tar;

		const float eer_tar = extractInfo(
			roc,
			std::vector<float>(target_fars, target_fars + target_far_cnt),
			closest_far,
			closest_tar);

		std::cout << "\nfull roc table:" << std::endl;
		std::cout << "eer_tar: " << eer_tar << std::endl;

		for(int i = 0; i < target_far_cnt; ++i)
		{
			printf("target far: %.5f%%  closest far: %.5f%% (%d/%d) tar: %.5f%%\n",
				target_fars[i] * 100.f,
				closest_far[i] * 100.f,
				int(closest_far[i] * mismatch_scores.size()),
				(int) mismatch_scores.size(),
				closest_tar[i] * 100.f);
		}


	}
	catch(const pbio::Error &e)
	{
		std::cerr << "facerec exception catched: '" << e.what() << "' code: " << std::hex << e.code() << std::endl;
	}
	catch(const std::exception &e)
	{
		std::cerr << "exception catched: '" << e.what() << "'" << std::endl;
	}

	return 0;
}
