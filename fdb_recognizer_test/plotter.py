## @file    plotter.py
## @brief   Script for plotting
## @author  Dmitry Matrokhin
## @date    15-November-2015

import sys
import matplotlib.pyplot as plt

## @brief make roc curve from scores list
## @param i_scores in, scores list
## @return ROC-curve
def makeRocCurve(i_scores):
   matchScores = i_scores[0]
   mismatchScores = i_scores[1]
   thresholds = sorted(matchScores + mismatchScores)

   rocCurveFars = []
   rocCurveTars = []
   for threshold in thresholds:
      truePosCnt = sum(i > threshold for i in matchScores)
      falsePosCnt = sum(i > threshold for i in mismatchScores)

      rocCurveTars.append(truePosCnt / len(matchScores))
      rocCurveFars.append(falsePosCnt / len(mismatchScores))
   return (rocCurveFars, rocCurveTars)

## @brief plot ROC-curve
## @param i_rocCurve in, ROC-curve list
def plotRocCurve(i_rocCurve):
   plt.figure()
   x, y = i_rocCurve
   plt.plot(x, y, 'r.', markersize = 5)

   plt.xlabel('Far')
   plt.ylabel('Tar')
   plt.title(r'$\mathrm{Roc\ curve:}$')
   plt.axis([-0.01, 1.01, -0.01, 1.01])
   plt.grid(True)

## @brief plot ROC-curve in loglog scale
## @param i_rocCurve in, ROC-curve list
def plotLogRocCurve(i_rocCurve):
   plt.figure()
   x, y = i_rocCurve
   y = [1 - i for i in y]
   plt.plot(x, y, 'b-')

   plt.xscale('log', nonposx='clip')
   plt.yscale('log', nonposx='clip')

   plt.xlabel('log(far)')
   plt.ylabel('log(1 - tar)')
   plt.title(r'$\mathrm{Roc\ curve (logarithmic):}$')
   plt.axis('auto')
   plt.grid(True)

## @brief plot histogram from scores
## @param i_scores in, scores list
def plotHist(i_scores):
   plt.figure()
   plt.hist(i_scores, 50, color=['green', 'red'], label = ['matches', 'mismatches'])
   plt.legend()
   plt.xlabel('Score')
   plt.ylabel('Count')
   plt.title(r'$\mathrm{Histogram\ of\ scores:}$')
   plt.grid(True)


if __name__ == '__main__':
   if len(sys.argv) != 3:
      print('Usage: python plotter.py <path to scores> <path to ROC-curve>')
   else:
      scoresFile = open(sys.argv[1])
      scores = []
      for line in scoresFile:
         scores.append(list(map(float, line.split())))
      scoresFile.close()

      rocCurve = makeRocCurve(scores)

      rocCurveFile = open(sys.argv[2], 'w+')
      for dim in rocCurve:
         for val in dim:
            rocCurveFile.write(str(val) + ' ')
         rocCurveFile.write('\n')
      rocCurveFile.close()

      # plotting
      plotHist(scores)
      plotRocCurve(rocCurve)
      plotLogRocCurve(rocCurve)

      plt.show()