/// @file Logger.cpp
/// @brief Contains declaration for class CLogger.
///        This class used for logging to logFile.log file
/// @author Dmitry Matrokhin
/// @date Created on: 01-11-2015

#include "Logger.h"

//////////////////////////////////////////////////////////////////////////
CLogger::CLogger(const std::string& i_logFile)
   : fout(i_logFile, std::ios::out)
{
}

//////////////////////////////////////////////////////////////////////////
void CLogger::log(const std::string& i_message)
{
   time_t secs = time(NULL);
   tm timeinfo;
   localtime_s(&timeinfo, &secs);
   char buf[256];
   asctime_s(buf, &timeinfo);
   std::string timeString(buf);
   timeString.pop_back();
   fout << timeString << ":   " << i_message << std::endl;
}
