## @file    imageConverter.py
## @brief   Script for converting 4 channels image to 3 channels image.
## @author  Dmitry Matrokhin
## @date    15-November-2015
import sys
from PIL import Image

## @brief convert image from 4 channel to 3 channel
## @param i_pathToImage in, path to image
def convertImage(i_pathToImage):
   img = Image.open(i_pathToImage)
   if img.mode != 'RGBA':
      return
   img.load()
   resImage = Image.new('RGB', img.size, (255, 255, 255))
   resImage.paste(img, mask = img.split()[-1])

   resImage.save(i_pathToImage, 'JPEG', quality = 80)

if __name__ == '__main__':
   if len(sys.argv) != 1:
      print('Usage: python imageConverter.py <path to image>')
   else:
      convertImage(sys.argv[1])